#include "util.h"
#include <fstream>

using namespace std;

vector<client> clienti;

// Mesaj de eroare
void error(char *msg)
{
  perror(msg);
  exit(0);
}


int main(int argc, char *argv[])
{
  int i, n, accept_len;
  struct sockaddr_in serv_addr, listen_addr, accept_addr;
  socklen_t length_listen = sizeof(listen_addr);
  int file ;
  char nume_fisier[BUFLEN];
  char buffer[BUFLEN];
  stringstream ss (stringstream::in | stringstream::out);
  stringstream info (stringstream::in | stringstream::out);
  stringstream inf (stringstream::in | stringstream::out);
  stringstream help (stringstream::in | stringstream::out);
  memset(buffer, 0, BUFLEN);
  
  //initializez structurile de server si structura de listen
  memset((char *) &serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(atoi(argv[5]));
  inet_aton(argv[4], &serv_addr.sin_addr);

  memset((char *) &listen_addr, 0, sizeof(listen_addr));
  listen_addr.sin_family = AF_INET;
  listen_addr.sin_port = htons(atoi(argv[3])); 
  listen_addr.sin_addr.s_addr = INADDR_ANY; 

  //verific daca am trimis indeajuns de multi parametrii scriptului
  if (argc < 6)
  {
    fprintf(stderr,"Usage %s client_name client_folder port_client server_address server_port\n", argv[0]);
    exit(0);
  }
  //construiesc fisierul de log pentru respectivul utilizator
  sprintf(nume_fisier,"%s.log",argv[1]);
  //cout <<nume_fisier<<endl;
  creat(nume_fisier,S_IRUSR  );
  file = open(nume_fisier,O_RDWR);
  if (file>0){
    cout <<"[client]Ai deschis cu succes fisierul log: "<<nume_fisier<<endl;
  }

  //Fac verificarile pentru socket si restul,exact ca in laborator
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0){
    error((char *)"-5: Eroare la citire de pe socket");
    write(file,"-5: Eroare la citire de pe socket\n",strlen("-5: Eroare la citire de pe socket\n"));
}
  if (connect(sockfd,(struct sockaddr*) &serv_addr,sizeof(serv_addr)) < 0)
    {
      error((char *)"-4 Eroare la conectare");
      write(file,"-4 Eroare la conectare\n",strlen("-4 Eroare la conectare\n"));
  
  }
  listen_sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (listen_sockfd < 0){
    //Nu stiam ce eroare sa dau aici dar am pus-o pe aceasta
    error((char *)"-5:Eroare la citire de pe socket");
    write(file,"-5:Eroare la citire de pe socket\n",strlen("-5:Eroare la citire de pe socket\n"));
  }

  if (bind(listen_sockfd, (struct sockaddr *) &listen_addr, sizeof(struct sockaddr)) < 0){
    error((char *)"ERROR on binding client's listen_socket");
    write(file,"ERROR on binding client's listen_socket\n",strlen("ERROR on binding client's listen_socket\n"));
}
  listen(listen_sockfd, MAX_CLIENTS);

  
  
  if (getsockname(listen_sockfd, (struct sockaddr *) &listen_addr, &length_listen) == -1)
    {
    error((char *)"ERROR getting socket name\n");
    write(file,"ERROR getting socket name\n",strlen("ERROR getting socket name\n"));
    }
  char cuvant[BUFLEN];
  sprintf(cuvant,"Nume: %s\nPort pe care asculta: %s\nDirector partajat: %s\n",argv[1],argv[3],argv[2]);
  write(file,cuvant,strlen(cuvant));
  // connect Nume_client listen_port_lcient
  sprintf(buffer,"connect %s %d", argv[1], ntohs(htons(atoi(argv[3]))) );

  n = send(sockfd,buffer,strlen(buffer), 0);

  memset(buffer, 0, BUFLEN);
  if ((n = recv(sockfd , buffer , sizeof(buffer),0)) !=0){
    if (n<0){
      //verific sa nu fac vreo greseala la primirea ,esajului de conectare
      error((char *)"ERROR in recv");
      write(file,"ERROR in recv\n",strlen("ERROR in recv\n"));
    }

    if (n>0){
        //Afisez faptul ca respectivul client s-a conectat cu succes la server
        //Si scriu asta in fisier
        cout <<"[client]Client " << buffer << " successfully to server\n";
        memset(cuvant,0,BUFLEN);
        sprintf(cuvant,"Clientul %s s-a conectat cu succes la server\n",buffer);
        write(file,cuvant,strlen(cuvant));
    }
  }
  else{
    close(sockfd);
    error((char *)"Client stopping");
    write(file,"Clientul se opreste\n",strlen("Clientul se opreste\n"));
  }

  FD_ZERO(&read_fds);
  FD_ZERO(&tmp_fds);

  FD_SET(listen_sockfd, &read_fds);
  fdmax = listen_sockfd;

  FD_SET(0, &read_fds);
  FD_SET(sockfd, &read_fds);

  //main loop
  while(1)
  {
    tmp_fds = read_fds;

    if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1)
      error((char *)"ERROR in select");

    for (i = 0; i <= fdmax; ++i)
    {
      if (FD_ISSET(i, &tmp_fds))
      {
        if (i == 0 )
        {
          //citesc de la tastatura
          memset(buffer, 0 , BUFLEN);
          fgets(buffer, BUFLEN-1, stdin);
          cout <<"[client]Ce faci baiatuleeee in sfarsit iti merge"<<endl;
          //TODO nu uita sa stergi prostia de mai sus
//          cout <<buffer <<endl;
          if (strcmp (buffer,"quit\n")==0){
            cout<<"aici ai comanda quit"<<endl;
            write(file,"Ai scris comanda quit\n",strlen("Ai scris comanda quit\n"));
            exit(0);
          }        
          if(strncmp(buffer,"listclients",strlen("listclients"))==0){
              // Trimit un string de forma "listclients nume_client_initiator"
             memset(buffer, 0, BUFLEN);
            sprintf(buffer, "listclients %s", argv[1]);

            int n = send(sockfd, buffer, strlen(buffer), 0);
            //Trimit serverului ce trebuie si astept sa imi dea ceva inapoi
            cout<<"ai trimis comanda listclients"<<endl;
            memset(buffer, 0, BUFLEN);
            if ((n = recv(sockfd, buffer, sizeof(buffer), 0)) > 0) {
              char comenzile[BUFLEN];
              ss << buffer;
              ss >> comenzile;
              char *buffer2;
              buffer2 = (buffer + strlen("clienti"));
              //afisez stringul rezultat de la server
              if (strncmp(comenzile, "clienti",strlen("clienti")) == 0){
                cout <<"[client]Rezultatul comenzii listclients este:\n" << buffer2 << endl;
                char altul[BUFLEN];
                sprintf(altul,"Rezultatul comenzii listclients este %s:\n", buffer2);
                write(file,altul,strlen(altul));
              }
            
            }     
            
          }
          //afisez fisierele partajate de un utilizator
          if (strncmp(buffer,"getshare",strlen("getshare"))==0){
            cout << "[client] Ai comanda gethsare";
            char utilizator[BUFLEN];
            int cap;
            sscanf(buffer,"%*s %s",utilizator);
            memset(buffer,0,BUFLEN);
            sprintf(buffer,"getshare %s %s",utilizator,argv[1]);
            cap = send(sockfd,buffer,sizeof(buffer),0);
            if (cap>0){
              cout<<"[client]Ai trimis cu succes comanda getshare"<<endl;
              memset(buffer,0,BUFLEN);
              int ni;
              if ((n = recv(sockfd, buffer, sizeof(buffer), 0)) > 0) {
                char comenzi[BUFLEN];
                char *bufferx;
                help << bufferx;
                help >> comenzi;
                //cout << buffer;
                bufferx = (buffer+strlen("shared-filesshared"));
                if (strncmp(buffer,"shared-filesshared",strlen("shared-filesshared"))==0){
                  cout << "[client]Rezultatul comenzii getshare este "<< endl;
                  cout << bufferx << endl;
                  write(file,bufferx,strlen(bufferx));
                }

                if (strncmp(buffer,"underlapping",strlen("underlapping"))==0) {
                  cout << "[client]-1 Client inexistent "<<endl;
                  write(file,"[client]-1 Client inexistent\n",strlen("[client]-1 Client inexistent\n"));
                }
            }


          }
        }
          //Partajez un fisier pentru un anumit utlizator
          if (strncmp(buffer,"sharefile",strlen("sharefile"))==0){

            cout<<"Ai comanda sharefile"<<endl;
            char fisier[BUFLEN];
            sscanf(buffer,"%*s %s",fisier);
            memset (buffer,0,BUFLEN);
            sprintf(buffer,"sharefile %s %s",argv[1],fisier);
            //sharefile nume_utilizator fisier_partajat
                  ifstream fis;
    //sprintf(buffer, "%s%s%s", director, "//", param1.c_str());
            string filepath;
            filepath.append(argv[2]);
            filepath.append("/");
            filepath.append(fisier);

            fis.open(filepath.c_str());
            if (!fis.is_open())
            {
              cout << "client: Fisierul nu poate fi deschis\n";
      
          }
          else{
            fis.close();


            int np= send(sockfd,buffer,strlen(buffer),0);
            if (n>0){
              write(file,"Ai trimis sharefile\n",strlen("Ai trimis sharefile\n"));
              cout << "[client]Ai trimis comanda sharefile "<<endl;
              int mp;
              if ((mp = recv(sockfd,buffer,sizeof(buffer),0))>0){
                char out[BUFLEN];
                inf << buffer;
                info >> out;
                if(strncmp(buffer,"Succes",strlen("Succes"))==0){
                  cout << "[client] SUcces" << endl;
                  write(file,"SUcces\n",strlen("Succes\n"));
                }
                if (strncmp(buffer,"overlapping",strlen("overlapping"))==0) {
                  cout << "[client]-2 Fisier inexistent "<<endl;
                  write(file,"[client]-2 Fisier inexistent\n",strlen("[client]-2 Fisier inexistent\n"));
                }
              }
            }
          }
          }                 
          //Afisez informatiile unui client
          if (strncmp(buffer,"infoclient",strlen("infoclient")) == 0){
            char destinatie[BUFLEN];
            memset (destinatie , 0 , BUFLEN);
            sscanf(buffer,"%*s %s",destinatie);
            memset(buffer,0,BUFLEN);
            //cout << destinatie<<endl;
            sprintf(buffer,"infoclient %s %s",argv[1],destinatie);
            //cout<<buffer<<endl;
            int n = send (sockfd , buffer,strlen(buffer),0 );
            if (n>0){
              write(file,"Ai trimis comanda quit\n",strlen("Ai trimis comanda quit\n"));
              cout <<"[client]ai trimis comanda infoclient " <<endl;
              int m ;
              if ((m=recv(sockfd,buffer,sizeof(buffer),0))>0){
                  char coutput[BUFLEN];
                  info << buffer;
                  info >> coutput;
             //     cout <<buffer <<"este bufferul"<<endl;
               //   cout <<"Ai ajung in if"<<endl;
                  char *buffer3;
                  buffer3 = (buffer + strlen ("clientul"));

                  if (strncmp(coutput,"clientul",strlen("clientul")) == 0){
                    char altul2[BUFLEN];
                    sprintf(altul2,"Rezultatul comenzii infoclient este: %s \n", buffer3);
                    cout <<"[client]Rezultatul comenzii infoclient este: \n" << buffer3 <<endl;
                    write(file,altul2,strlen(altul2));
                  }
                  else {
                    write(file,"-1:Client inexistent\n",strlen("-1:Client inexistent\n"));
                    cout << "-1:Client inexistent"<<endl;
                  }
                  memset(buffer,0,BUFLEN);
              }
            }
            
            else if (n<0){
                error((char *)"ERROR in sending");   
            }

          }  
        }   
        

        else if (i == sockfd)
        {
          // Am primit ceva de la server
          memset(buffer, 0, BUFLEN);
          if ((n = recv(sockfd, buffer, sizeof(buffer), 0)) <= 0)
          {
            if (n!=0){
              close(sockfd);
              close(listen_sockfd);
            }
            
            //in cazul in care serverul imi trimite quit sau nu
            cout <<"[client]Fie esti deconectat fie se inchide serverul"<<endl;
            write(file,"Fie esti deconectat fie se inchide serverul\n",strlen("Fie esti deconectat fie se inchide serverul\n"));
            exit(0);
          }
          else
          {
            // Am primit instiintare de disconnect
            if (strncmp(buffer, "disconnected", strlen("disconnected")) == 0)
            {
              cout <<"[client]Clientul a fost deconectat ";
              cout << endl;
              write(file,"Clientul a fost deconectat\n",strlen("Clientul a fost deconectat\n"));
              exit(0);
            }

            }
        }

        else if (i == listen_sockfd)
        {
          // a venit ceva pe socketul de ascultare = o nouă conexiune
          // acțiune: accept()
          accept_len = sizeof(accept_addr);
          if ((newsockfd = accept(listen_sockfd, (struct sockaddr *)&accept_addr, (socklen_t *)&accept_len)) == -1)
          {
            error((char *)"ERROR in accept");
            write(file,"ERROR in accept\n",strlen("ERROR in accept \n"));
          }
          else
          {
            //adaug noul socket intors de accept() la multimea descriptorilor de citire
            FD_SET(newsockfd, &read_fds);
            if (newsockfd > fdmax)
            {
              fdmax = newsockfd;
            }
          }
        }

      }
    }



  }

  close(listen_sockfd);
  close(sockfd);

  close(file);
  return 0;
}
