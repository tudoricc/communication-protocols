#include "header.h"

#define BUFLEN 256
#define MAX_CLIENTS 20
#define CMDSZ 100
#define NAMESZ 100



typedef struct client_
{
  char nume[BUFLEN];  // Nume client
  int port;   // Port ascultare
  struct sockaddr_in adresa;  // Adresa clientului
}client;
