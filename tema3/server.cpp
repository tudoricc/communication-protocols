#include "header.h"

#define MAX_CLIENTS 5
#define BUFLEN 256
#define NAMESZ 100
#define CMDSZ 100

using namespace std;

//am creeat o structura initiala pentru a memora in ea clientii conectati
typedef struct clientserver_
{
    string nume;    // Nume client
    int port;       // Port de ascultare
    struct sockaddr_in cli_addr;  // Adresa clientului
    int srv_sockfd;     // Socket pe care e conectat la server
    vector<string> shared_files;    // Fisiere partajate
}clientserver;

vector<clientserver> clienti;


//functia de afisare a erorii
void error(char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int portno, length_client;
    char buffer[BUFLEN];
    struct sockaddr_in serv_addr, cli_addr;
    int n, i;
    int maxim_clienti = 20;
    stringstream shared (stringstream::in | stringstream::out);
    shared << "shared-files";
    if (argc < 2)
    {
        fprintf(stderr,"Usage : %s port\n", argv[0]);
        exit(1);
    }

    
    
    portno = atoi(argv[1]);
    //initializez structurile mele pentru server
    memset((char *) &serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // foloseste cli_addr IP a masinii
    serv_addr.sin_port = htons(portno);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error((char *)"ERROR opening socket at server");

    
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr)) < 0)
        error((char *)"ERROR on binding");

    listen(sockfd, maxim_clienti);
    //golim multimea de descriptori de citire (read_fds) si multimea tmp_fds
    FD_ZERO(&read_fds);
    FD_ZERO(&tmp_fds);
    FD_SET(sockfd, &read_fds);
    fdmax = sockfd;

  
    FD_SET(0, &read_fds);

    // main loop
    while (1)
    {

        tmp_fds = read_fds;
        if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1)
            error((char *)"ERROR in select");

        for(i = 0; i <= fdmax; i++)
        {
            if (FD_ISSET(i, &tmp_fds))
            {
                if (i == sockfd)
                {
                    
                    // actiunea serverului: accept()
                    length_client = sizeof(cli_addr);
                    int newsockfd  = accept(sockfd, (struct sockaddr *)&cli_addr, (socklen_t *)&length_client);
                    if (newsockfd == -1){
                        //verific daca un utilizator s-a conectat la server
                        cout << "conexiune noua de la "<<inet_ntoa(cli_addr.sin_addr)<<" de pe portul "<< ntohs(cli_addr.sin_port);
                        cout<< " socketul clientului este "<<newsockfd<<endl;
                    }
                    else
                    {
                        //adaug noul socket intors de accept() la multimea descriptorilor de citire
                        FD_SET(newsockfd, &read_fds);
                        if (newsockfd > fdmax)
                        {
                            fdmax = newsockfd;
                        }
                    }
                }


                else if (i == 0)
                {
                    //citesc de la tastatura
                    //pe mine ma intereseaza doar 2 comenzi de la tastatura,quit si status.
                    memset(buffer, 0 , BUFLEN);
                    fgets(buffer, BUFLEN-1, stdin);

                     if (strncmp(buffer, "status\n",strlen( "status\n")) == 0)
                        {
                        int ceva;
                        for (ceva = 0; ceva < clienti.size(); ++ceva)
                        {
                            cout <<"Numele: "<< clienti[ceva].nume << " de pe ip-ul ";
                            cout << inet_ntoa(clienti[ceva].cli_addr.sin_addr) << " si de pe portul " << clienti[ceva].port;
                            cout << endl;
           
                        }

        
                    }
                    if (strncmp(buffer, "quit\n",strlen("quit\n")) == 0)
                     {
                         cout<< "Comanda este Quit"<<endl;
                         //aici apesi doar odata ENTER

                        for (int i = 0; i <= fdmax; ++i)
                             if ( i != fileno(stdin) && FD_ISSET(i, &tmp_fds))
                             {
                                 memset(buffer, 0, BUFLEN);
                                sprintf(buffer, "Serverul s-a inchis");

                                 int n = send(i, buffer, strlen(buffer), 0);
                
                                close(i);
                                FD_CLR(i, &read_fds); // scoatem din multimea de citire socketul
                             }
                        exit(0);
                    }
                    /*else{
                        cout << "Here tubbbbby tubyyyyyy" << endl;
                    }*/
                }
                else
                {
                    //actiunea serverului: recv()
                    memset(buffer, 0, BUFLEN);
                    if ((n = recv(i, buffer, sizeof(buffer), 0)) <= 0)
                    {
                        if (n<0){
                            int j = 0;
                            for ( j = 0; j < clienti.size(); ++j)
                            {
                                if (clienti[j].srv_sockfd == i)
                                 clienti.erase(clienti.begin() + j);
                            }
                            close(i);
                            FD_CLR(i, &read_fds); // scoatem din multimea de citire socketul
                        }
                        
                    }

                    else
                    { //recv intoarce >0
                        //Vad daca utilizatorul este unic sau nu si fac ce trebuie
                        cout<< "Am primit de la clientul de pe socketul "<< i<<" mesajul: "<<buffer<<endl;
                        char nume[BUFLEN];
                        char comanda[CMDSZ];
                        clientserver aux_client;
                        int port;
                        sscanf(buffer,"%s %*s", comanda);
                       

                          if ( strcmp(comanda, "connect") == 0)
                             {
                           

                            sscanf(buffer, "%*s %s %d", nume, &port);
                            cout << "Numele este: " << nume << " ,portul este: " << port;
                            cout << endl;

                             string name(nume);
                             //clienti.push_back(name);
                            aux_client.nume = name;
                            aux_client.port = port;
                            aux_client.cli_addr = cli_addr;
                            aux_client.srv_sockfd = sockfd;
                            aux_client.shared_files.clear();
       
                            int socket = i;
                            for ( int m = 0; m < clienti.size(); ++m)
                            {
                                if (clienti[m].nume.compare(name) == 0) {
                                    memset(buffer, 0, BUFLEN);
                                    sprintf(buffer, "Disconnected exista un client cu acest nume");
                                    sprintf(buffer, "Asa ca incearca cu alt nume");
                                    
                                    cout << "A aparut un conflict de nume"<<endl;
                                    cout <<"Deja exista un utilizator cu numele: " << name<<endl;
                                    
                                    int n = send(socket, buffer, strlen(buffer), 0);
                                    close(socket);
                                    FD_CLR(socket, &read_fds); // scoatem din multimea de citire socketul
                
                                }
                            }
                            clienti.push_back(aux_client);
                            
                            memset(buffer, 0, BUFLEN);
                            sprintf(buffer, "connected");

                            int n = send(i, buffer, strlen(buffer), 0);
                            
       
                         }
                         else if (strncmp(comanda,"getshare",strlen("getshare"))==0){
                            cout << "Getshare" <<endl;
                            char from[BUFLEN];
                            char to[BUFLEN];
                            sscanf(buffer,"%*s %s %s",to,from);
                            cout << "Comanda vine de la " <<from << endl;
                            

                            int ig;
                            int jg;
                            int conti = 0;

                            for ( ig = 0 ; ig <clienti.size();ig++){
                                if (strcmp(clienti[ig].nume.c_str(),from) == 0){
                                    shared << "shared " ;
                                    for (jg = 0 ; jg < clienti[ig].shared_files.size();jg++){
                                        shared <<" "<< clienti[ig].shared_files[jg] << " " <<endl;
                                    }
                                    cout <<"Intra si aici"<<endl;
                                    string buf;

                                    buf = shared.str();
                                    memset(buffer, 0, BUFLEN);
                                    buf.copy(buffer, buf.length());

                                cout << "Bufferul este "<<buffer << endl;
                                    int n = send(i, buffer, strlen(buffer), 0);
                           
                                }
                                else{
                                    conti++;
                                }

                            }
                            if (conti == clienti.size()){
                                cout <<"Nu gasesc acest utilizator"<<endl;
                                memset(buffer,0,BUFLEN);
                                sprintf(buffer,"Undergoing");
                                int msa;
                                msa = send(i,buffer,strlen(buffer),0);
                                if(msa>0){
                                    cout << "Ai trimis corect raspunsul la comanda getshare"<<endl;
                                }
                            }

                         }
                         //iau comanda de sharefile si fac ce trebuie
                         else if (strncmp(comanda,"sharefile",strlen("sharefile")) == 0){
                            cout << "Sharefile"<<endl;
                            char user[BUFLEN];
                            char file[BUFLEN];
                            sscanf(buffer,"%*s %s %s",user,file);
                          //  cout<<"Esti 1"<<endl;
                            int ip;
                            int jp;
                            cout <<"Comanda vine de la: " << user <<endl;
                            int count = 0;
                           // cout <<"Esti 2"<<endl;
                            for (ip = 0 ; ip<clienti.size();ip++){
                                if (strcmp(clienti[ip].nume.c_str(),user)==0){
                                    for (jp= 0 ; jp<clienti[ip].shared_files.size();jp++){
                                        if(clienti[ip].shared_files[jp].compare(file)==0){
                                            cout << "You already have this file shared"<<endl;
                                            memset(buffer,0,BUFLEN);
                                            sprintf(buffer,"overlapping");
                                            cout <<buffer<<endl;
                                            int kp;
                                            kp = send(i, buffer, strlen(buffer), 0);
                                            if (kp<0){
                                                error((char*)"ERROR in send");
                                            }    
                                            else{
                                                cout << "Ai gasit acest fisier deja sharuit"<<endl;

                                            }
                                            cout<<endl;
                                            cout <<endl;
                                        }
                                        else{
                                            count++;
                                        }
                                    }
                                    if (count == clienti[ip].shared_files.size()){
                                        //adica nu mai ai acest fisier sharuit
                                        cout << "You don;t have it shared yet"<<endl;
                                        clienti[ip].shared_files.push_back(file);                                           
                                        memset(buffer,0,BUFLEN);
                                        sprintf(buffer,"Succes");
                                        cout << buffer << endl;
                                        int klp;
                                        klp = send (i , buffer,strlen(buffer),0);
                                        

                                    }
                                }

                            }

                         }
                         //afisez clientii
                        else if ( strcmp(comanda, "listclients") == 0 )
                         {                        
                            stringstream ss (stringstream::in | stringstream::out);
                             ss << "clienti";
                             int k;
                             for (k = 0; k < clienti.size(); k++){
                                 ss << " " << clienti[k].nume<<endl;
                                 
                             }
                            string buf;
                            buf = ss.str();
                             memset(buffer, 0, BUFLEN);
                            buf.copy(buffer, buf.length());

                             
                            int n = send(i, buffer, strlen(buffer), 0);
                           
                        }
                        else if ( strcmp (comanda, "infoclient") == 0){
                            char nume_client[BUFLEN];
                            char nume_destinatie[BUFLEN];
                            sscanf(buffer,"%*s %s %s",nume_client,nume_destinatie);
                            string nume_final;

                            int count=0;
                            for ( int i = 0 ; i<clienti.size() ; i++){
                                if (clienti[i].nume.compare(nume_destinatie)==0){
                                    cout << "Exista  acest client pe server"<<endl;
                                    //Daca vrei sa vezi asta apasa inca odata ENTER
                                    sprintf(buffer,"clientul %s %s %d",clienti[i].nume.c_str(),inet_ntoa(clienti[i].cli_addr.sin_addr),clienti[i].port);
                                    //TODO nu uita sa faci tot carnatul de mai sus mai scurt
                                    

                                }
                                else{
                                    count++;
                                }
                            }
                            if ( count == clienti.size()){
                                cout << "Clientul cautat nu poate fi gasit" << endl;
                                memset(buffer, 0, BUFLEN);
                                sprintf(buffer, "client-inexistent");

                                int n = send(i, buffer, strlen(buffer), 0);
                            }
                            else {
                                cout << buffer <<endl;
                                    int n ;
                                    n = send ( i , buffer , strlen (buffer),0);
                                    if (n>0){
                                        cout <<"S-a trimis "<<endl;
                                    }
                            }

                        } 

                    }
                }
            }
        }
    }


    close(sockfd);

    return 0;
}

