#ifndef LIB
#define LIB

#define ACK_T1		"ACK Corect"
#define ACK_T2		"ACK Eronat"
#define ACK_T3		"Timeout"

#define MSGSIZE		60


typedef unsigned char byte;


/*
Structura unui frame pentru send
*/
typedef struct {
  int len;
  char payload[1400];
} msg;
/*
Structura unui frame
*/
typedef struct {
	byte SEQ_NO_RECEIVE;
	byte Checksum;
} frame_recv;

typedef struct {
  byte SEQ_NO_SEND;
  byte Checksum;
  byte payload[61];
} frame_send;
/*
  Asta este functia pentru calcularea partiatii
  pentru fiecare bit.
  In general pentr un mesaj ar trebui sa fac XOR
  la aceasta functie pe fiecare byte din numarul nostru
*/
byte calc_checksum(frame_send a,int dimensiune) {
//  byte mask = 1;
  byte par = 0;
  int i;
  for(i=0; i <= dimensiune-1; i++) {
    par = par ^ a.payload[i];
  }
  return par;
}
void init(char* remote,int remote_port);
void set_local_port(int port);
void set_remote(char* ip, int port);
int send_message(const msg* m);
int recv_message(msg* r);

#endif