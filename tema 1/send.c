#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

// sys/types.h, sys/stat.h si fcntl.h.
#define HOST "127.0.0.1"
#define PORT 10000
#define dim_buffer 61
#define true 1
int random_generator(){
	
	srand(rand());
	return rand()%60;
}

int main(int argc,char** argv){
	//byte SEQ_NO_EXPECTED = 0;
	frame_send s,rec ; //ce avem de trimis
	char buffer[dim_buffer];//unde punem  datele 
	msg event,mesaj;//evenimentul pe care il astepti,gen ack
	byte next_frame_to_send = 0;
	//FILE* fd = NULL;
	//int nr_citit=1;
	//sprintf(s.payload,"%s","abcd");
	int numar =random_generator();
	int i = 0 ;
	int x;
	time_t t;
	time(&t);
	FILE *f = fopen("log.txt" ,"w"); //fisierul in care incercam sa scriem
	init(HOST,PORT);
	if ( argc != 2){
		printf("[send.c]Eroarele de parametrii");
		//daca nu primesc indeajunse argumente in linia de comanda
	}
	else{
		x = open(argv[1],O_RDONLY);
		if(x<0){
			printf("[send.c]Eroare deschidere fisier");
		}
		else{
			read(x,buffer,numar);
			//de unde ->in ce ->cate
			//Sclose(x);
		}	
			
	} 
	int n =1;
	int y = open("log.txt",O_WRONLY);
	while (n<10){
		//pun in frame-ul de send informatiile necesare
		//in payload pun buffer-ul,in secventa frame-ul necesar 
		//iar in checksum rezultatul  in urma functiei de calculare a checksum
		memcpy(s.payload,buffer,numar);
		s.SEQ_NO_SEND=next_frame_to_send;
		s.Checksum = calc_checksum(s,numar) ;

		//eu vreau sa trimit un mesaj si de aceea nu pot trimite un frame de
		// send si de aceea copiez tot frame-ul in payload-ul unui mesaj[msg]
		//si in lungime pun lungimea frame-ului
		memcpy(&mesaj.payload,&s,sizeof(s));
		mesaj.len = sizeof(s)+1;
		//trimit mesajul 
		send_message(&mesaj);
			//write(y,);
		/*Aici scriu in fisierul meu de log ce ar trebui sa contina
		*Din pacate pe calculatorul meu nu imi afisa deloc in fisier si nu 
		*inteleg deloc de ce .De aceea am facut si afisari in terminal care 
		*afiseaza cam tot aceleasi lucruri
		*/
		fprintf(f, "%s   \n[sender]Am trimis urmatorul pachet:\n",ctime(&t));
		fprintf(f,"[sender]Seq_No: %d \n",next_frame_to_send);
		fprintf(f,"[sender]Payload: %s\n",buffer);
		fprintf(f,"[sender]Checksum: %d \n",s.Checksum);
		fprintf(f,"-----------------------------------------------------\n");


		//Dupa ce am trimis un mesaj ii resetez campul si resetez si 
		//sirul in care citesc
		memset(&s.payload[0],0,sizeof(s.payload));
		memset(&buffer[0],0,sizeof(buffer));


		printf("[send.c]Sending message\n");
		//recv_message(&event);
		//if(event.payload.SEQ_NO_EXPECTED == next_frame_to_send){
		if (recv_message(&event) >0 )
			{
					
				/*Daca primesc un mesaj corect eu ii copiez continutul sau 
				intr-un frame si verific daca secventa sa este egala cu
				frame-ul asteptat*/
				memcpy(&rec,&event.payload,event.len);
				if(rec.SEQ_NO_SEND == next_frame_to_send){
					//printf("^esti aici^\n");
					int n = random_generator();
						/*Citesc urmatoarele n[random] caractere din fisierul
						meu din care tot citesc,dupa aceea incrementez 
						frame-ul de trimis */
						read(x,buffer,n);
						//printf("^numarul este %d^\n",n);
						next_frame_to_send++;
						printf("[sender]Bufferul citit  este: %s\n",buffer);
						numar= n;
						printf("---------------------------------------\n\n");

					}
				else if (rec.SEQ_NO_SEND != next_frame_to_send){
					/*Daca secventa sa nu este egala cu frame-ul trimis ,
					atunci voi retrimite acel mesaj sperand sa se schimbe ceva
					*/
					printf("[sender]ERoare\n");
					send_message(&mesaj);
				}
			 	}
	
	
	n++;
	}
return 0;



}