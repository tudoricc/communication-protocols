#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lib.h"
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
// sys/types.h, sys/stat.h si fcntl.h.
#define HOST "127.0.0.1"
#define PORT 10001
#define dim_buffer 61
#define true 1
void dec_bin(long int num) {
	long int rem[50],i=0,length=0;
	while(num>0){
 		rem[i]=num%2;
 		num=num/2;
 		i++;
 		length++;
 	}
}
int main (int argc,char** argv){
	byte SEQ_NO_NEXT = 0;
	msg s;
	frame_send recv;
	msg event;
	//msg ack ;
	byte frame_expected=0;
	int fd,i;
	char a[32];
	init(HOST,PORT);
	//fd = open("log.txt",O_RDWR);
	time_t t;
	FILE *f = fopen("log.txt", "w");
	//deschid fisierul in care ar trebui sa scriu
	time(&t);
	while(true){
		
		if(recv_message(&event)< 0 ){
			perror("[receive.c]receive eroor");
			return -1;

		}
		else{
			/*printf("--->Message received in receive<---\n");
			printf("---->content:%x<------\n",&event.payload);
*/			/*Eu primesc un mesaj dar am nevoie de un frame si de aceea copiez
			continutul intr-un frame noi pentru a face putea continua conform
			algoritmului*/
			memcpy(&recv,&event.payload,event.len);

			int num = recv.Checksum;
			long int rem[50],i=0,length=0;
			while(num>0){
 				rem[i]=num%2;
 				num=num/2;
 				i++;
 				length++;
 			}
 			/*Aici scriu in fisierul meu de log ce ar trebui sa contina
			*Din pacate pe calculatorul meu nu imi afisa deloc in fisier si nu 
			*inteleg deloc de ce .De aceea am facut si afisari in terminal care 
			*afiseaza cam tot aceleasi lucruri
			*/
			fprintf(f,"%s \n",ctime(&t));	
 			fprintf(f,"[receiver]Am primit urmatorul pachet: \n");
 			fprintf(f,"[receiver]Seq_No: %d\n",frame_expected);
 			fprintf(f,"[receiver]Payload: %s\n",recv.payload);
 			fprintf(f,"[receiver]Checksum: ");
 			for (i = 0 ; i< length ;i++){
 				fprintf(f,"%ld",rem[i]);//calculez din integer in binar
 			}
 			fprintf(f,"--------------------------------------\n\n\n");
					
			
 			/*for (i = 0 ; i< length ;i++){
 				printf("%ld",rem[i]);
 			}*/
			printf("[recv.c]--->Message received in receive<---\n");
			printf("[recv.c]---->content:%s<------\n",recv.payload);
			printf("[recv.c]---->Checksum:");
			for (i = 0 ; i< length ;i++){
 				printf("%ld",rem[i]);//from int to binary
 			}
 			printf("<------\n");
 			printf("---------------------------------------\n\n");
			byte checksum = calc_checksum(recv,sizeof(recv.payload));
			
			if( recv.Checksum == checksum){
			//	if(recv.SEQ_NO_SEND==SEQ_NO_NEXT){
				//printf("--->ai intrat aici in receive<-----\n");
				//write(fd,"Ceva cumva",10);
				/*write(fd,"[",1);
				write(fd,ctime(&t),20);
				2rite(fd,"] [receiver] Am primit urmatorul pachet",40);
*/				//fprintf(f, "ceva\n" );
				//write(fd,recv.payload ,sizeof(recv.payload)+1);
				

				//Conform algoritmului eu trebuie sa retrimit un mesaj cu 
				//frame-ul asteptat si asta fac in continuare
				//dar din nou trebuie sa trimit un mesaj si de aceea copiez 
				//continutul unui frame in el
				frame_recv frame ;
				frame.SEQ_NO_RECEIVE =(byte) frame_expected;
				frame.Checksum = (byte) frame_expected;
				memcpy(&s.payload,&frame , sizeof(frame));
				s.len = sizeof(frame)+1;

				//trimti mesajul
				send_message(&s);

				//incrementez frame-ul asteptat
				frame_expected++;
					

			//	}
			}
			else{
				//Daca nu priemsc frame-ul asteptat retrimit acelasi frame
				//decrementez frame-ul pentru a-l retrimite pe cel anterior 
				//pentru ca a fost trimit gresit
				frame_recv frame;
				frame.SEQ_NO_RECEIVE = (byte)frame_expected-1;
				frame.Checksum = (byte) frame_expected - 1;
				memcpy(&s.payload,&frame ,sizeof(frame));
				s.len = sizeof(frame)+1;
				send_message(&s);
			}

		}
		//break;
		}
	fclose(f);
	return 0;
}